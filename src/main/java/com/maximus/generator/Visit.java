package com.maximus.generator;

import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Repository
public class Visit {
    private LocalDateTime entry;
    private LocalDateTime exit;

    public Visit(LocalDate date) {

    }

    public Visit() {
    }

    @Override
    public String toString() {
        return "WE: " + getEntry().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm"))
                + " | WY: " + getExit().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm"));
    }

    public LocalDateTime getEntry() {
        return entry;
    }

    public void setEntry(LocalDateTime entry) {
        this.entry = entry;
    }

    public LocalDateTime getExit() {
        return exit;
    }

    public void setExit(LocalDateTime exit) {
        this.exit = exit;
    }

}
