package com.maximus.generator.tests;

import com.maximus.generator.Month;
import com.maximus.generator.Visit;
import com.maximus.generator.VisitsGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class VisitsGeneratorTest {

    VisitsGenerator generator;

    @Before
    public void init() {
        generator = new VisitsGenerator();
    }
//    @Autowired
//    VisitsGenerator generator;

    @Test
    public void shouldGenerateSomeDate() {
        Visit visit = generator.generateSingleVisit(LocalDate.now(), LocalTime.now());
        Assert.assertNotNull(visit);
        System.out.println(visit.toString());
    }

    @Test
    public void shouldGenerateFourElementDatesList() {
        List<Visit> visits = generator.generateVisitsList(LocalDate.now(), 4);
        Assert.assertEquals(4, visits.size());
        visits.forEach(System.out::println);
    }

    @Test
    public void visitShouldBeValid() {
        Visit visit = new Visit();
        visit.setEntry(LocalDateTime.now());
        visit.setExit(visit.getEntry().plusMinutes(1));
        Assert.assertTrue(generator.validateVisit(visit));
    }

    @Test
    public void visitShouldNotBeValid() {
        Visit visit = new Visit();
        visit.setExit(LocalDateTime.now());
        visit.setEntry(visit.getExit().plusMinutes(1));
        Assert.assertFalse(generator.validateVisit(visit));
    }

    @Test
    public void monthShouldContainProperDaysValue(){
        LocalDate date = LocalDate.now();
        Month month = generator.generateMonth(date);
        GregorianCalendar calendar = new GregorianCalendar(
                month.getDate().getYear(),
                month.getDate().getMonth().getValue(),
                month.getDate().getDayOfMonth());
        int daysInGeneratedMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        calendar = new GregorianCalendar(
                date.getYear(),
                date.getMonth().getValue(),
                date.getDayOfMonth()
        );
        int expectedDaysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        System.out.println(month);
        Assert.assertEquals(expectedDaysCount, daysInGeneratedMonth);
    }

}
