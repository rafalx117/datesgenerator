package com.maximus.generator;

import java.time.LocalDate;
import java.util.List;

public class Month {
    private List<Day> days;
    private LocalDate date;

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(date);
        builder.append("\n");
        days.forEach(builder::append);
        return builder.toString();
    }
}
