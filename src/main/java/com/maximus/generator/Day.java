package com.maximus.generator;

import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class Day {
    private LocalDate date;
    private List<Visit> visits;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getName() {
        return date.getDayOfWeek().name();
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(date);
        builder.append("\n");
//        visits.forEach(builder::append); //WERSJA POPRAWIONA PRZEZ INTELLIJ
        visits.forEach(visit -> {
            builder.append(visit);
            builder.append("\n");
        });

        return builder.toString();
    }
}
