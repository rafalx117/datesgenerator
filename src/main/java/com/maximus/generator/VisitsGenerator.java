package com.maximus.generator;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class VisitsGenerator implements IVisitsGenerator {

    private Random random = new Random();
    private int minimalDelay = 2;
    private int maximalDelay = 4;
    private LocalTime defaultLocalTime = LocalTime.of(4,0);
    private LocalTime lastVisitTime = LocalTime.of(18, 30);

    @Override
    public Visit generateSingleVisit(LocalDate day, LocalTime time) {

        Visit visit = new Visit();
        int randomHourDelay = random.nextInt(maximalDelay) + minimalDelay;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, day.getYear());
        calendar.set(Calendar.MONTH, day.getMonthValue()-1);
        calendar.set(Calendar.DAY_OF_MONTH, day.getDayOfMonth());
//        int hour = random.nextInt(randomHourDelay) + time.getHour();
//        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.HOUR_OF_DAY, time.getHour() + randomHourDelay);
        calendar.set(Calendar.MINUTE, random.nextInt(60));

        visit.setEntry(LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault()));

        //generate exit date

        if(calendar.get(Calendar.MINUTE) > 30){
            incrementHour(calendar);
            calendar.set(Calendar.MINUTE, random.nextInt(60));
        }
        else {
            drawMinutesWithinSameHour(calendar);
        }

        visit.setExit(LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault()));

        return visit;
    }

    private void drawMinutesWithinSameHour(Calendar calendar) {
        calendar.set(Calendar.MINUTE, random.nextInt(60 - calendar.get(Calendar.MINUTE)) + calendar.get(Calendar.MINUTE));
    }

    private void incrementHour(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR) + 1);
    }

    @Override
    public List<Visit> generateVisitsList(LocalDate date, int count) {
        List<Visit> visits = new ArrayList<>();

        LocalTime tempDateTime = defaultLocalTime;

        for (int i = 0; i < count; i++) {
            Visit visit = generateSingleVisit(date, tempDateTime);

            //TODO RM: przerobić tak, żeby validateVisit rzucało wyjątek
            if(!validateVisit(visit)){
                i--;
                continue;
            }

            visits.add(visit);


            tempDateTime = visit.getExit().toLocalTime();
        }


        return visits;
    }

    @Override
    public Month generateMonth(LocalDate day) {
        List<Day> days = new ArrayList<>();
        int monthValue = day.getMonth().getValue();
        GregorianCalendar calendar = new GregorianCalendar(day.getYear(), day.getMonth().getValue(), day.getDayOfMonth());
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        LocalDate date = day;

        for(int i = day.getDayOfMonth(); i <= daysInMonth; i++){
            int visitsCount = new Random().nextInt(4) + 2; //mmaximum visitsCount value (with 4 and 2) is 5
            Day tempDay = new Day();
            tempDay.setDate(date);
            tempDay.setVisits(generateVisitsList(date, visitsCount));
            days.add(tempDay);
            date = date.plusDays(1);
        }

        Month month = new Month();
        month.setDate(day);
        month.setDays(days);

        return month;
    }

    @Override
    public boolean validateVisit(Visit visit) {
        return visit.getExit().isAfter(visit.getEntry());
    }
}
