package com.maximus.generator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface IVisitsGenerator {

    Visit generateSingleVisit(LocalDate day, LocalTime time);

    List<Visit> generateVisitsList(LocalDate day, int count);

    boolean validateVisit(Visit visit);

    Month generateMonth(LocalDate day);

}
